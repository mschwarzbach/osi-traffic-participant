/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef ITRAFFICPARTICIPANTMODEL_H
#define ITRAFFICPARTICIPANTMODEL_H

#include <osi_sensorview.pb.h>
#include <osi_trafficupdate.pb.h>

#include <chrono>
#include <map>
#include <string>

namespace osi_traffic_participant
{
class ITrafficParticipantModel
{
public:
  ITrafficParticipantModel() = default;
  virtual ~ITrafficParticipantModel() = default;

  /// @brief Identifies the OSI entity from osi3::GroundTruth entities in the provided \p sensor_view and
  ///        provides a new state with an osi3::TrafficUpdate.
  /// @param delta_time  Delta time between the step in milliseconds.
  /// @param sensor_view osi3::SensorView containing osi3::GroundTruth containing all the entities
  ///                    (including the controlled entity).
  virtual osi3::TrafficUpdate Update(const std::chrono::milliseconds& delta_time,
                                     const osi3::SensorView& sensor_view) = 0;

  /// @brief Indicates that the controlled entity will be updated or not.
  /// @return true if the entity will not be updated in future cycles, i.e. when the road has ended. False otherwise.
  virtual bool HasFinished() const = 0;
};

}  // namespace osi_traffic_participant

#endif  // ITRAFFICPARTICIPANTMODEL_H
